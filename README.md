# OpenRGB Nvidia usage plugin

Script that allows you to change presets based on your nvidia GPU usage

## ToDo:
- [ ] Extend to other GPU manufacturers

## Dependencies
```
linux
python
nvidia-smi
openrgb
```
## Installation
```
git clone https://gitlab.com/k1ake/openrgb-nvidia-usage-plugin
cd openrgb-nvidia-usage-plugin
python keyboard_rgb_gpu.py --install
```

## Manual Installation:
- Clone repo: ```git clone https://gitlab.com/k1ake/openrgb-nvidia-usage-plugin```
- Copy script to any dir you want
- Change preset names and percentages in presets_and_percents 
- Copy .service to user dir ```~/.config/systemd/user/```
- Change path to script and to python if
- Reload systemd daemons ```systemctl --user daemon-reload```
- Start service ```systemctl --user start keyboard_rgb_gpu.service```
- Enable service ```systemctl --user enable keyboard_rgb_gpu.service```

## Setting up
- Create some presets for your devices with OpenRGB
- In ```.config/keyboard_rgb_gpu/config.json``` add your percentage values and preset names
- Run ```keyboard_rgb_gpu --restart``` to update configuration within
