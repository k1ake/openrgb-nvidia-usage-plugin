#!/usr/bin/env python
import subprocess
import os
import time
import sys
import json


def help():
    print(f"""OpenRGB Nvidia usage plugin:
Usage: keyboard_rgb_gpu.py [options]

Options:
    -h, --help
        Displays that message

    --restart
        Sources config file and restarts daemon

    --install
        Installs script for user

    --install-force
        Forcefully reinstalls everything 
        Only works within cloned repo

    --oneshot
        Runs program ones and exists
""")


def write_service(systemd_path: str, bin_path: str):
    python_path = subprocess.Popen(["which", "python"], stdout=subprocess.PIPE).communicate()[
        0].decode().replace("\n", "")
    service = f"""[Unit]
Description=Changing OpenRGB preset on keyboard usage changes

[Service]
ExecStart={python_path} {bin_path+"keyboard_rgb_gpu"}
Restart=on-failure

[Install]
WantedBy=default.target
"""
    with open(systemd_path+"keyboard_rgb_gpu.service", "w") as f:
        f.write(service)


def is_installed() -> bool:
    answer = subprocess.Popen(["systemctl","--user", "status", "keyboard_rgb_gpu.service"], stdout=subprocess.PIPE).communicate()[0].decode()
    if "enabled" not in answer:
        return False
    return True


def install(force=False):
    if not force and is_installed():
        return True
    config_path = os.path.expanduser("~/.config/keyboard_rgb_gpu/")
    bin_path = os.path.expanduser("~/.local/bin/")
    systemd_path = os.path.expanduser("~/.config/systemd/user/")

    from shutil import copyfile
    if not os.path.exists(bin_path):
        os.makedirs(bin_path, exist_ok=True)
    if not os.path.exists("keyboard_rgb_gpu.py"):
        print("Script not found! Launch --install from script's dir")
        return
    copyfile("keyboard_rgb_gpu.py", bin_path+"keyboard_rgb_gpu")
    os.chmod(bin_path+"keyboard_rgb_gpu", 0o777)

    if not os.path.exists("config.json"):
        print("Config not found! Launch --install from script's dir")
        return
    if not os.path.exists(config_path):
        os.makedirs(config_path)

    with open("config.json", "r") as f:
        config = json.load(f)
    config["openrgb_dir"] = os.path.expanduser(config["openrgb_dir"])
    config["tmp_file"] = os.path.expanduser(config_path+"current_preset")
    with open(config_path+"config.json", "w") as f:
        json.dump(config, f)

    if not os.path.exists(systemd_path):
        os.makedirs(systemd_path, exist_ok=True)
    write_service(systemd_path, bin_path)

    restart(config)


def restart(config: dict):
    subprocess.run(config["systemctl_command"] + ["daemon-reload"])
    subprocess.run(config["systemctl_command"] +
                   ["enable", "keyboard_rgb_gpu.service"])
    update_preset("\n", config)
    subprocess.run(config["systemctl_command"] +
                   ["restart", "keyboard_rgb_gpu.service"])


def get_preset_name(val: int, config: dict) -> str:
    preset = config["presets_and_percents"]["0"]
    for k, v in config["presets_and_percents"].items():
        if val >= int(k):
            preset = v
    return preset


def check_preset_name(preset: str, config: dict):
    preset_file = config["openrgb_dir"]+preset
    return os.path.isfile(preset_file+".orp") or os.path.isfile(preset_file+".ors")


def update_preset(preset: str, config: dict):
    with open(config["tmp_file"], "w") as f:
        f.write(preset)

# True - same as now
# False - differs


def is_cur_preset_enabled(preset: str, config: dict) -> bool:
    if not os.path.isfile(config["tmp_file"]):
        update_preset(preset, config)
        return False
    with open(config["tmp_file"], "r") as f:
        saved_preset = f.read()
    return saved_preset == preset


def main(config: dict):
    value = int(subprocess.Popen(
        config["gpu_command"], stdout=subprocess.PIPE).communicate()[0].decode())
    preset = get_preset_name(value, config)
    if not check_preset_name(preset, config):
        return
    if is_cur_preset_enabled(preset, config):
        return
    update_preset(preset, config)
    subprocess.Popen(config["openrgb_command"]+[preset])


if __name__ == "__main__":
    if ("--install" in sys.argv):
        install()
        exit()
    if ("--install-force" in sys.argv):
        install(force=True)
        exit()
    try:
        with open(os.path.expanduser("~/.config/keyboard_rgb_gpu/config.json")) as f:
            CONFIG = json.load(f)
    except FileNotFoundError:
        print("Config not found, run script again with --install")
        exit()
    if len(sys.argv) > 1:

        if ("-h" in sys.argv or "--help" in sys.argv):
            help()

        if ("--restart" in sys.argv):
            restart(CONFIG)

        if ("--oneshot" in sys.argv or CONFIG["time_interval"] < 0):
            main(CONFIG)
            print("\n")
        exit()

    while True:
        main(CONFIG)
